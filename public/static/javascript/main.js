/**
 * By: Karl Löfquist
 */

/*
*	Function that runs when the entire restaurant.html has been loaded
*/
function init() {

	const bodyElement = document.getElementsByTagName('body')[0];

	// Fetching json data from the data-endpoint (data.json)
	fetch('./data')
		.then(response => response.json())
		// Sends recieved json to the printData function
	  	.then(data => printData(data, bodyElement));

}

// Adding event listener to window object to call the init() function when the index page has been loaded
window.addEventListener('load', init);

/**
 * Function for printing the fetched data from https://sv.wikipedia.org/wiki/Intelligenskvot
 * @param  {[JSON Array]} data  [JSON Array with data from https://sv.wikipedia.org/wiki/Intelligenskvot]
 * @param  {[DOM Element]} bodyElement [Reference to the body element in index html]
 * @return {[null]}
 */
function printData(data, bodyElement) {

	// Template literal for table base structure
	let tableBaseStructure = `

		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">IQ</th>
		      <th scope="col">Andel</th>
		      <th scope="col">Procent</th>
		    </tr>
		  </thead>
		  <tbody id="tableBody">
		    
		  </tbody>
		</table>

	`;

	// Append the base structure to the body element
	bodyElement.insertAdjacentHTML('beforeend', tableBaseStructure);

	data.forEach((entry) => {
		let dataEntry = `

		    <tr>
		      <th scope="row">${entry.IQ}</th>
		      <td>${entry.Status}</td>
		      <td>${entry.Procentavbefolkningen}</td>
		    </tr>

		`;

		// Append each table row of fetched data to table body with id 'tablebody'
		document.getElementById('tableBody').insertAdjacentHTML('beforeend', dataEntry);

	});

	let chart = `
		<div id="piechart"></div>
	`;

	// Append chart div to body element
	bodyElement.insertAdjacentHTML('beforeend', chart);

	// Load google chart
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(() => {

		// RegExp for getting a number from percentage
		let r = /\d+/;
		let dataArray = [];

		// Push data of correct format into array
		data.forEach(element => {
			dataArray.push([element.IQ.replace(':', ''), parseInt(element.Procentavbefolkningen.match(r)[0])]);
		});

		// Setting graph data
		let graphData = google.visualization.arrayToDataTable([
			['IQ', 'Percentage'],
			dataArray[0],
			dataArray[1],
			dataArray[2],
			dataArray[3],
			dataArray[4],
			dataArray[5],
			dataArray[6]
		]);

		// Optional; add a title and set the width and height of the chart
		let options = {'title':'IQ Graph', 'width':550, 'height':400};

		// Display the chart inside the <div> element with id="piechart"
		let chart = new google.visualization.PieChart(document.getElementById('piechart'));
		chart.draw(graphData, options);

	});

}