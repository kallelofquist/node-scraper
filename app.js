/*
*	By: Karl, Carl-Michael and Martin
*/

// Necessary constants
const express = require('express');
const app = express();
const cheerio = require('cheerio');
const request = require('request');
const path = require('path');
const data = require('./data.json');
const fs = require('fs');

const { PORT = 8080 } = process.env;
let obj = {};
let array = [];

// Sets handy usages in app
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Endpoint for root ('/')
app.get('/', (req, res) => {
	// Return file (index.html)
	return res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Endpoint for data ('/data')
app.get('/data', (req, res) => {
	// Return JSON-data
	return res.status(200).json(data);
});

request('https://sv.wikipedia.org/wiki/Intelligenskvot', (error, response, html) => {
        const $ = cheerio.load(html);

        //finds table through class name wikitable, take the last one (since it is the table we want), goes through it. 
        $('.wikitable').last().each(function(i, elem) { 
            
            const table = $(elem)
            //create a for loop that loop through table
            //begins with i=1, so that the first row is not includes, since it does not includes info. we want
            for(i = 1; i < table.find('tr').length; i++) {
            
                //stores each table row in an array
                let tempArray = [];
        
                //we want to store everything in an object but we want to split the object before each th
                for(j = 0; j < table.find('th').length; j++) {
                    let temp =  table.find('tr').eq(i).children().eq(j).text();
                    temp = temp.trim();
                    tempArray.push(temp);
                }

                //creates the jsonobject and store the information   
                let JSONObject = {
                    IQ: tempArray[0],
                    Status: tempArray[1],
                    Procentavbefolkningen: tempArray[2]
                };

                //push to the array
                array.push(JSONObject);
            }      
    });

    //writes to data.json
    let json = JSON.stringify(array, null, 2);
    fs.writeFile(__dirname + "/data.json", json, ()=> null);
});

app.listen(PORT, ()=> console.log(`Server started on port ${PORT}...`));
